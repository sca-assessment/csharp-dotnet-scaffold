using System;
using System.Text.Encodings.Web;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace ScaffoldProject
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRouting();
        }

        public void Configure(IApplicationBuilder app)
        {
            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Welcome to the vulnerable app! Go to /process?input=<your_input> to test.");
                });

                endpoints.MapGet("/process", async context =>
                {
                    var input = context.Request.Query["input"];
                    if (string.IsNullOrEmpty(input))
                    {
                        await context.Response.WriteAsync("Please provide an input query parameter.");
                        return;
                    }

                    // Vulnerable usage of System.Text.Encodings.Web
                    string encodedInput = JavaScriptEncoder.Default.Encode(input);
                    Console.WriteLine("Encoded user input: " + encodedInput);

                    // Safe usage of Newtonsoft.Json
                    var logEntry = new { UserInput = input, EncodedInput = encodedInput };
                    string jsonLog = JsonConvert.SerializeObject(logEntry);
                    Console.WriteLine("Log entry: " + jsonLog);

                    await context.Response.WriteAsync("Processed input: " + encodedInput);
                });
            });
        }

        public static void Main(string[] args)
        {
            var host = new WebHostBuilder()
                .UseKestrel()
                .UseStartup<Startup>()
                .Build();

            host.Run();
        }
    }
}