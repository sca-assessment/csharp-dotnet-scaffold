# Use the official .NET SDK image
FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build

# Set the working directory
WORKDIR /app

# Copy the csproj and restore as distinct layers
COPY ScaffoldProject.csproj .
COPY packages.config .

# Restore dependencies
RUN dotnet restore ScaffoldProject.csproj

# Copy the rest of the application code
COPY . .

# Build the application
RUN dotnet build ScaffoldProject.csproj --no-restore -c Release -o /app/build

# Use the official ASP.NET Core runtime image
FROM mcr.microsoft.com/dotnet/aspnet:5.0 AS runtime

# Set the working directory
WORKDIR /app

# Copy the built application
COPY --from=build /app/build .

# Expose port 80
EXPOSE 80

# Set the entry point
ENTRYPOINT ["dotnet", "ScaffoldProject.dll"]